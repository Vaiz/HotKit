#include "selectitem.h"
#include "ui_selectitem.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

SelectItem::SelectItem(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::SelectItem)
{
    ui->setupUi(this);

    tabelModel.setTable("items_view");
    ui->tableView->setModel(&tabelModel);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelModel.select();

    ui->tableView->setVisible(false);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);

    QSqlQuery query;
    if(query.exec("SELECT name FROM hobby;"))
    {
        while (query.next())
            ui->hobbyComboBox->addItem(query.value("name").toString());

        query.prepare("SELECT name FROM sub_hobby WHERE sub_hobby.hobby_id = ( "
                      "SELECT id FROM hobby WHERE name = "
                      ":category );");
        query.bindValue(":category", ui->hobbyComboBox->currentText());

        if(query.exec())
        {
            while (query.next())
                ui->subHobbyComboBox->addItem(query.value("name").toString());
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    loadImages();

    connect(ui->hobbyComboBox, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(on_HobbyChanged(QString)));
    connect(ui->subHobbyComboBox, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(on_SubHobbyChanged(QString)));
}

SelectItem::~SelectItem()
{
    delete ui;
}

void SelectItem::on_cancelButton_clicked()
{
    this->close();
}

QString SelectItem::getItemId() const
{
    return itemId;
}

QString SelectItem::getImageId() const
{
    return imageId;
}

void SelectItem::on_HobbyChanged(const QString &_hobby)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM sub_hobby WHERE sub_hobby.hobby_id = ( "
                  "SELECT id FROM hobby WHERE name = "
                  ":category );");
    query.bindValue(":category", _hobby);

    if(query.exec())
    {
        ui->subHobbyComboBox->clear();

        while (query.next())
            ui->subHobbyComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

void SelectItem::on_SubHobbyChanged(const QString &)
{
    loadImages();
}

void SelectItem::loadImages()
{
    ui->listWidget->clear();

    QSqlQuery queryHobby;
    queryHobby.prepare("SELECT id FROM hobby WHERE name = :name ;");
    queryHobby.bindValue(":name", ui->hobbyComboBox->currentText());

    if(queryHobby.exec())
    {
        if (queryHobby.next())
        {
            QString hobbyId = queryHobby.value("id").toString();

            QSqlQuery querySubHobby;
            querySubHobby.prepare("SELECT id FROM sub_hobby\n"
                                  "WHERE sub_hobby.hobby_id = :hobbyId \n"
                                  "AND name = :name ;");
            querySubHobby.bindValue(":hobbyId", hobbyId);
            querySubHobby.bindValue(":name", ui->subHobbyComboBox->currentText());

            if(querySubHobby.exec())
            {
                if (querySubHobby.next())
                {
                    QString subHobbyId = querySubHobby.value("id").toString();

                    QSqlQuery queryImages;
                    queryImages.prepare("SELECT id, path FROM images WHERE sub_hobby_id = :sub_hobby_id ;");
                    queryImages.bindValue(":sub_hobby_id", subHobbyId);

                    if(queryImages.exec())
                    {
                        while (queryImages.next())
                        {
                            QString imagePath = queryImages.value("path").toString();
                            QString imageId = queryImages.value("id").toString();
                            ui->listWidget->addItem(new QListWidgetItem(QIcon(imagePath), imageId));
                        }
                    }
                    else
                    {
                        qDebug() << queryImages.lastError().text();
                    }
                }
            }
            else
            {
                qDebug() << querySubHobby.lastError().text();
            }
        }
    }
    else
    {
        qDebug() << queryHobby.lastError().text();
    }
}

void SelectItem::on_okButton_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    if(indexes.isEmpty())
    {
        qDebug() << "Выберите чехол!";
        return;
    }
    else
    {
        QModelIndex index = tabelModel.index(indexes.at(0).row(), 4);
        if(tabelModel.data(index).toInt() == 0)
        {
            qDebug() << "Данные чехлы закончились!";
            return;
        }
        else
        {
            QModelIndex index = tabelModel.index(indexes.at(0).row(), 0);
            itemId = tabelModel.data(index).toString();
        }
    }

    QList<QListWidgetItem *> images = ui->listWidget->selectedItems();
    if(images.isEmpty())
    {
        qDebug() << "Выберите изображение!";
        return;
    }
    else
    {
        imageId = images[0]->text();
    }

    needUpdate = true;
    this->close();
}
