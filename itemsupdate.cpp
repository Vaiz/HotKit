#include "itemsupdate.h"
#include "ui_itemsupdate.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

ItemsUpdate::ItemsUpdate(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::ItemsUpdate)
{
    ui->setupUi(this);

    QSqlQuery query;
    if(query.exec("SELECT name FROM item_type;"))
    {
        while (query.next())
            ui->itemTypeComboBox->addItem(query.value("name").toString());

        query.prepare("SELECT name FROM sub_item_type WHERE sub_item_type.item_type_id = ( "
                      "SELECT id FROM item_type WHERE name = "
                      ":category );");
        query.bindValue(":category", ui->itemTypeComboBox->currentText());

        if(query.exec())
        {
            while (query.next())
                ui->subItemTypeComboBox->addItem(query.value("name").toString());
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    if(query.exec("SELECT * FROM technology;"))
    {
        while (query.next())
            ui->technologyComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    connect(ui->itemTypeComboBox, SIGNAL(currentIndexChanged(QString)),
                                         this, SLOT(on_ItemTypeChanged(QString)));
}

ItemsUpdate::~ItemsUpdate()
{
    delete ui;
}

void ItemsUpdate::on_ItemTypeChanged(const QString &_itemType)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM sub_item_type WHERE sub_item_type.item_type_id = ( "
                  "SELECT id FROM item_type WHERE name = "
                  ":category );");
    query.bindValue(":category", _itemType);

    if(query.exec())
    {
        ui->subItemTypeComboBox->clear();

        while (query.next())
            ui->subItemTypeComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

void ItemsUpdate::setId(QString _id)
{
    id = _id;
    QSqlQuery query;
    if(query.exec("SELECT * FROM items_view WHERE id = " + id))
    {
        if(query.numRowsAffected() == 1)
        {
            query.next();
            ui->itemTypeComboBox->setCurrentText(query.value("Категория").toString());
            ui->subItemTypeComboBox->setCurrentText(query.value("Подкатегория").toString());
            ui->technologyComboBox->setCurrentText(query.value("Технология").toString());
            ui->countSpinBox->setValue(query.value("Количество").toInt());
            ui->costSpinBox->setValue(query.value("Цена").toInt());
        }
        else
        {
            qDebug() << "Клиент с таким id не найден.";
            this->close();
        }
    }
    else
        qDebug() << query.lastError().text();
}

void ItemsUpdate::on_updateButton_clicked()
{
    QSqlQuery query;
    query.prepare("call Hotkit.update_items( :id, :sub_item_type, :tecnology, :count, :cost);");
    query.bindValue(":id", id);
    query.bindValue(":sub_item_type", ui->subItemTypeComboBox->currentText());
    query.bindValue(":tecnology", ui->technologyComboBox->currentText());
    query.bindValue(":count", ui->countSpinBox->value());
    query.bindValue(":cost", ui->costSpinBox->value());

    if(query.exec())
    {
        needUpdate = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

void ItemsUpdate::on_cancelButton_clicked()
{
    this->close();
}
