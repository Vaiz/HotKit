#ifndef ORDERSADD_H
#define ORDERSADD_H

#include <QVector>
#include <QPair>

#include "subwindowstemplate.h"
#include "selectclient.h"
#include "selectitem.h"

namespace Ui {
class OrdersAdd;
}

class OrdersAdd : public SubWindowsTemplate
{
    Q_OBJECT

    SelectClient *selectClientForm;
    SelectItem *selectItemForm;
    QString clientId;
    QVector< QPair<QString, QString> > itemsId; // id item and id image

public:
    explicit OrdersAdd(QWidget *parent = 0);
    ~OrdersAdd();

private slots:
    void on_cancelButton_clicked();
    void on_clientsAddButton_clicked();
    void on_SelectClientClose(bool _needUpdate);
    void on_addOrderButton_clicked();
    void on_SelectItemClose(bool _needUpdate);
    void on_addButton_clicked();
    void on_clearOrdersButton_clicked();

private:
    Ui::OrdersAdd *ui;
};

#endif // ORDERSADD_H
