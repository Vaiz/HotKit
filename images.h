#ifndef IMAGES_H
#define IMAGES_H

#include <QWidget>
#include "QListWidget"

#include "imagesadd.h"
#include "subwindowstemplate.h"

namespace Ui {
class Images;
}

class Images : public SubWindowsTemplate
{
    Q_OBJECT

    ImagesAdd *imagesAddForm;

    QListWidgetItem *lastSelectedImage;

public:
    explicit Images(QWidget *parent = 0);
    ~Images();

private slots:
    void on_addButton_clicked();
    void on_ImagesAddClose(bool _needUpdate);
    void on_deleteButton_clicked();
    void on_imageClick(QListWidgetItem * _item);

private:
    Ui::Images *ui;

    void loadImages();
};

#endif // IMAGES_H
