#-------------------------------------------------
#
# Project created by QtCreator 2015-04-26T13:43:45
#
#-------------------------------------------------

QT       += core gui sql

#unix:LIBS += -lmysqld
#win32:LIBS += libpqdll.lib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HotKit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    clients.cpp \
    clientsadd.cpp \
    clientsupdate.cpp \
    items.cpp \
    subwindowstemplate.cpp \
    itemsadd.cpp \
    itemsupdate.cpp \
    orders.cpp \
    images.cpp \
    imagesadd.cpp \
    ordersadd.cpp \
    selectclient.cpp \
    selectitem.cpp \
    ordersupdate.cpp \
    statistics.cpp \
    pieview.cpp

HEADERS  += mainwindow.h \
    clients.h \
    clientsadd.h \
    clientsupdate.h \
    items.h \
    subwindowstemplate.h \
    itemsadd.h \
    itemsupdate.h \
    orders.h \
    images.h \
    imagesadd.h \
    ordersadd.h \
    selectclient.h \
    selectitem.h \
    ordersupdate.h \
    statistics.h \
    pieview.h

FORMS    += mainwindow.ui \
    clients.ui \
    clientsadd.ui \
    clientsupdate.ui \
    items.ui \
    itemsadd.ui \
    itemsupdate.ui \
    orders.ui \
    images.ui \
    imagesadd.ui \
    ordersadd.ui \
    selectclient.ui \
    selectitem.ui \
    ordersupdate.ui \
    statistics.ui

CONFIG += mobility c++11
MOBILITY =

