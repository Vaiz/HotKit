#include "ordersupdate.h"
#include "ui_ordersupdate.h"

#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>
#include <QDebug>

OrdersUpdate::OrdersUpdate(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::OrdersUpdate)
{
    ui->setupUi(this);

    QSqlQuery query;
    if(query.exec("SELECT name FROM parcell_state;"))
    {
        while (query.next())
            ui->stateComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

OrdersUpdate::~OrdersUpdate()
{
    delete ui;
}

void OrdersUpdate::on_cancelButton_clicked()
{
    this->close();
}

void OrdersUpdate::setId(QString _id)
{
    id = _id;

    QSqlQuery queryOrders;
    if(queryOrders.exec("SELECT * FROM orders_view WHERE id = " + id))
    {
        if(queryOrders.numRowsAffected() == 1)
        {
            queryOrders.next();
            ui->clientEdit->setText(queryOrders.value("Заказчик").toString());
            ui->stateComboBox->setCurrentText(queryOrders.value("Статус").toString());
            ui->costEdit->setText(queryOrders.value("Цена").toString());
        }
        else
        {
            qDebug() << "Заказ с таким id не найден.";
            this->close();
        }
    }
    else
        qDebug() << queryOrders.lastError().text();

    QSqlQuery queryItemsId;
    queryItemsId.prepare("SELECT item_id, image_id FROM orders_content\n"
                         "WHERE order_id = :order_id;");
    queryItemsId.bindValue(":order_id", id);
    if(queryItemsId.exec())
    {
        while(queryItemsId.next())
        {
            QString itemId = queryItemsId.value("item_id").toString();
            QSqlQuery queryItem;
            queryItem.prepare("SELECT * FROM items_view WHERE id = :id ;");
            queryItem.bindValue(":id", itemId);

            QString imageId = queryItemsId.value("image_id").toString();
            QSqlQuery queryImage;
            queryImage.prepare("SELECT path FROM images WHERE id = :id ;");
            queryImage.bindValue(":id", imageId);

            if(queryItem.exec() && queryImage.exec())
            {
                if(queryItem.next() && queryImage.next())
                {
                    QString category = queryItem.value("Категория").toString();
                    QString subCategory = queryItem.value("Подкатегория").toString();
                    QString technology = queryItem.value("Технология").toString();
                    QString fullText = category + " / " + subCategory + " / " + technology;

                    QString imagePath = queryImage.value("path").toString();

                    ui->listWidget->addItem(new QListWidgetItem(QIcon(imagePath), fullText));
                }
            }
            else
            {
                qDebug() << queryItem.lastError().text();
                qDebug() << queryImage.lastError().text();
            }
        }
    }
    else
        qDebug() << queryItemsId.lastError().text();
}

void OrdersUpdate::on_updateButton_clicked()
{
    QSqlQuery query;
    query.prepare("CALL update_orders_state(:id, :state);");
    query.bindValue(":id", id);
    query.bindValue(":state", ui->stateComboBox->currentText());

    if(query.exec())
    {
        needUpdate = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}
