#include "clientsupdate.h"
#include "ui_clientsupdate.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

ClientsUpdate::ClientsUpdate(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClientsUpdate)
{
    ui->setupUi(this);
    clientUpdated = false;

    QSqlQuery query;
    if(query.exec("SELECT * FROM sex;"))
    {
        while (query.next())
            ui->sexComboBox->addItem(query.value("name").toString());
    }
    else
        qDebug() << query.lastError().text();

    if(query.exec("SELECT * FROM relationship;"))
    {
        while (query.next())
            ui->relationshipsComboBox->addItem(query.value("name").toString());
    }
    else
        qDebug() << query.lastError().text();
}

ClientsUpdate::~ClientsUpdate()
{
    delete ui;
}

void ClientsUpdate::closeEvent(QCloseEvent* event)
{
    emit closed(clientUpdated);
    QWidget::closeEvent(event);
}

void ClientsUpdate::setId(QString _id)
{
    id = _id;
    QSqlQuery query;
    if(query.exec("SELECT * FROM clients_view WHERE id = " + id))
    {
        if(query.numRowsAffected() == 1)
        {
            query.next();
            ui->firstnameEdit->setText(query.value("Фамилия").toString());
            ui->secondnameEdit->setText(query.value("Имя").toString());
            ui->indexEdit->setText(query.value("Индекс").toString());
            ui->stateEdit->setText(query.value("Область").toString());
            ui->townEdit->setText(query.value("Город").toString());
            ui->addressEdit->setText(query.value("Адрес").toString());
            ui->ageSpinBox->setValue(query.value("Возраст").toInt());
            ui->sexComboBox->setCurrentText(query.value("Пол").toString());
            ui->relationshipsComboBox->setCurrentText(query.value("Семейное положение").toString());
        }
        else
        {
            qDebug() << "Клиент с таким id не найден.";
            this->close();
        }
    }
    else
        qDebug() << query.lastError().text();
}

void ClientsUpdate::on_cancelButton_clicked()
{
    this->close();
}

void ClientsUpdate::on_updateButton_clicked()
{
    QSqlQuery query;
    query.prepare("call Hotkit.update_client( :id,"
                  ":first_name, :second_name, :zip_code,"
                  ":state, :town, :address, :age, :sex,"
                  ":relationship);");
    query.bindValue(":id",  id);
    query.bindValue(":first_name",  ui->firstnameEdit->text());
    query.bindValue(":second_name",  ui->secondnameEdit->text());
    query.bindValue(":zip_code",  ui->indexEdit->text());
    query.bindValue(":state",  ui->stateEdit->text());
    query.bindValue(":town",  ui->townEdit->text());
    query.bindValue(":address",  ui->addressEdit->text());
    query.bindValue(":age",  ui->ageSpinBox->value());
    query.bindValue(":sex",  ui->sexComboBox->currentText());
    query.bindValue(":relationship",  ui->relationshipsComboBox->currentText());

    if(query.exec())
    {
        qDebug() << "Клиент успешно отредактирован.";
        clientUpdated = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
        QMessageBox messageBox;
        messageBox.setText(query.lastError().text());
        messageBox.open();
    }
}
