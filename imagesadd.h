#ifndef IMAGESADD_H
#define IMAGESADD_H

#include <QWidget>

#include "subwindowstemplate.h"

namespace Ui {
class ImagesAdd;
}

class ImagesAdd : public SubWindowsTemplate
{
    Q_OBJECT

public:
    explicit ImagesAdd(QWidget *parent = 0);
    ~ImagesAdd();

private slots:
    void on_HobbyChanged(const QString &_itemType);
    void on_cancelButton_clicked();

    void on_addImagesButton_clicked();

    void on_addButton_clicked();

    void on_clearImagesButton_clicked();

private:
    Ui::ImagesAdd *ui;
};

#endif // IMAGESADD_H
