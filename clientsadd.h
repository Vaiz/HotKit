#ifndef CLIENTSADD_H
#define CLIENTSADD_H

#include <QWidget>

namespace Ui {
class ClientsAdd;
}

class ClientsAdd : public QWidget
{
    Q_OBJECT

public:
    explicit ClientsAdd(QWidget *parent = 0);
    ~ClientsAdd();

private slots:
    void on_addButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::ClientsAdd *ui;
    void closeEvent(QCloseEvent* event);
    bool clientAdded;

signals:
    void closed(bool _clientAdded = false);
};

#endif // CLIENTSADD_H
