#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>

#include "clients.h"
#include "items.h"
#include "orders.h"
#include "images.h"
#include "statistics.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Clients *clientsForm;
    Items *itemsForm;
    Orders *ordersForm;
    Images *imagesForm;
    Statistics *statisticsForm;
    QSqlDatabase db;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_clientsButton_clicked();
    void on_ClientsClose();
    void on_itemsButton_clicked();
    void on_ItemsClose();
    void on_ordersButton_clicked();
    void on_OrdersClose();
    void on_imagesButton_clicked();
    void on_ImagesClose();
    void on_statisticButton_clicked();
    void on_StatisticsClose();

private:
    Ui::MainWindow *ui;
    void closeEvent(QCloseEvent* event);
};

#endif // MAINWINDOW_H
