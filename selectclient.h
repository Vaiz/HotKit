#ifndef SELECTCLIENT_H
#define SELECTCLIENT_H

#include <QSqlTableModel>

#include "subwindowstemplate.h"

namespace Ui {
class SelectClient;
}

class SelectClient : public SubWindowsTemplate
{
    Q_OBJECT

    QSqlTableModel tabelModel;
    QString clientId;

public:
    explicit SelectClient(QWidget *parent = 0);
    ~SelectClient();
    QString getClientId() const;

private slots:
    void on_cancelButton_clicked();
    void on_okButton_clicked();

private:
    Ui::SelectClient *ui;
};

#endif // SELECTCLIENT_H
