#ifndef ORDERS_H
#define ORDERS_H

#include <QtSql/QSqlTableModel>

#include "subwindowstemplate.h"
#include "ordersadd.h"
#include "ordersupdate.h"

namespace Ui {
class Orders;
}

class Orders : public SubWindowsTemplate
{
    Q_OBJECT

    QSqlTableModel tabelModel;

    OrdersAdd *ordersAddForm;
    OrdersUpdate *ordersUpdateForm;

public:
    explicit Orders(QWidget *parent = 0);
    ~Orders();

private slots:
    void on_addButton_clicked();
    void on_OrdersAddClose(bool _needUpdate);
    void on_deleteButton_clicked();
    void on_editButton_clicked();
    void on_OrdersUpdateClose(bool _needUpdate);

private:
    Ui::Orders *ui;
};

#endif // ORDERS_H
