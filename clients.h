#ifndef CLIENTS_H
#define CLIENTS_H

#include <QWidget>
#include <QtSql/QSqlTableModel>

#include "clientsadd.h"
#include "clientsupdate.h"

namespace Ui {
class Clients;
}

class Clients : public QWidget
{
    Q_OBJECT

    QSqlTableModel tabelModel;
    ClientsAdd *clientsAddForm;
    ClientsUpdate *clientsUpdateForm;

public:
    explicit Clients(QWidget *parent = 0);
    ~Clients();

private:
    Ui::Clients *ui;
    void closeEvent(QCloseEvent* event);

signals:
    void closed();

private slots:
    void on_addButton_clicked();
    void on_ClientsAddClose(bool _clientAdded);
    void on_ClientsUpdateClose(bool _clientUpdated);
    void on_deleteButton_clicked();
    void on_editButton_clicked();
};

#endif // CLIENTS_H
