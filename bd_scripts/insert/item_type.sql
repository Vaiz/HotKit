INSERT INTO Hotkit.item_type (`name`) VALUES 
            ('Телефоны'), 
            ('Документы'), 
            ('Другое'); 
            
SELECT id INTO @item_type_id FROM Hotkit.item_type WHERE name = 'Телефоны';
INSERT INTO Hotkit.sub_item_type (`item_type_id`,`name`) VALUES 
            (@item_type_id , 'iphone 4'), 
            (@item_type_id , 'iphone 5'), 
            (@item_type_id , 'iphone 6'), 
            (@item_type_id , 'iphone 6-plus'),
            (@item_type_id , 'ipad'), 
            (@item_type_id , 'ipad Air'),
            (@item_type_id , 'ipad mini'),
            (@item_type_id , 'ipod touch 5'),
            (@item_type_id , 'samsung galaxy S2'),
            (@item_type_id , 'samsung galaxy S3'),
            (@item_type_id , 'samsung galaxy S4'),
            (@item_type_id , 'samsung galaxy S5'),
            (@item_type_id , 'samsung galaxy Note 2'), 
            (@item_type_id , 'samsung galaxy Note 3'), 
            (@item_type_id , 'samsung galaxy Note 4'), 
            (@item_type_id , 'samsung galaxy S3 mini'),
            (@item_type_id , 'samsung galaxy S4 mini'), 
            (@item_type_id , 'samsung galaxy S5 mini'), 
            (@item_type_id , 'samsung galaxy ace 2'), 
            (@item_type_id , 'samsung galaxy ace 3'), 
            (@item_type_id , 'HTC one S'),
            (@item_type_id , 'HTC one X'), 
            (@item_type_id , 'HTC one M7'), 
            (@item_type_id , 'HTC one M8 mini'),
            (@item_type_id , 'HTC Sensation XL'), 
            (@item_type_id , 'Nokia Lumia 920'), 
            (@item_type_id , 'Sony Xperia SP'), 
            (@item_type_id , 'Sony Xperia Z'), 
            (@item_type_id , 'Sony Xperia Z1'), 
            (@item_type_id , 'Sony Xperia Z2'),
            (@item_type_id , 'Sony Xperia C3'),
            (@item_type_id , 'LG Nexus 4'),
            (@item_type_id , 'LG Nexus 5');
            
SELECT id INTO @item_type_id FROM Hotkit.item_type WHERE name = 'Документы';
INSERT INTO Hotkit.sub_item_type (`item_type_id`,`name`) VALUES 
            (@item_type_id , 'Зачетка'), 
            (@item_type_id , 'Паспорт'), 
            (@item_type_id , 'Студенческий'), 
            (@item_type_id , 'Военный билет');

SELECT id INTO @item_type_id FROM Hotkit.item_type WHERE name = 'Другое';
INSERT INTO Hotkit.sub_item_type (`item_type_id`,`name`) VALUES 
            (@item_type_id , 'Зажим'), 
            (@item_type_id , 'Кошелек женский'), 
            (@item_type_id , 'Портмоне'), 
            (@item_type_id , 'Чехол универсальный');
