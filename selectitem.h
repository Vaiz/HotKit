#ifndef SELECTITEM_H
#define SELECTITEM_H

#include <QSqlTableModel>

#include "subwindowstemplate.h"

namespace Ui {
class SelectItem;
}

class SelectItem : public SubWindowsTemplate
{
    Q_OBJECT

    QSqlTableModel tabelModel;

    QString itemId;
    QString imageId;

public:
    explicit SelectItem(QWidget *parent = 0);
    ~SelectItem();
    QString getItemId() const;
    QString getImageId() const;

private slots:
    void on_cancelButton_clicked();
    void on_HobbyChanged(const QString &_hobby);
    void on_SubHobbyChanged(const QString &_subHobby);

    void on_okButton_clicked();

private:
    Ui::SelectItem *ui;

    void loadImages();
};

#endif // SELECTITEM_H
