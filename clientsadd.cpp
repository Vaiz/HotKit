#include "clientsadd.h"
#include "ui_clientsadd.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

ClientsAdd::ClientsAdd(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClientsAdd)
{
    ui->setupUi(this);
    clientAdded = false;

    QSqlQuery query;
    if(query.exec("SELECT * FROM sex;"))
    {
        while (query.next())
            ui->sexComboBox->addItem(query.value("name").toString());
    }
    else
        qDebug() << query.lastError().text();

    if(query.exec("SELECT * FROM relationship;"))
    {
        while (query.next())
            ui->relationshipsComboBox->addItem(query.value("name").toString());
    }
    else
        qDebug() << query.lastError().text();
}

ClientsAdd::~ClientsAdd()
{
    delete ui;
}

void ClientsAdd::closeEvent(QCloseEvent* event)
{
        emit closed(clientAdded);
        QWidget::closeEvent(event);
}

void ClientsAdd::on_addButton_clicked()
{
    QSqlQuery query;
    query.prepare("call Hotkit.add_client( "
                  ":first_name, :second_name, :zip_code,"
                  ":state, :town, :address, :age, :sex,"
                  ":relationship);");
    query.bindValue(":first_name",  ui->firstnameEdit->text());
    query.bindValue(":second_name",  ui->secondnameEdit->text());
    query.bindValue(":zip_code",  ui->indexEdit->text());
    query.bindValue(":state",  ui->stateEdit->text());
    query.bindValue(":town",  ui->townEdit->text());
    query.bindValue(":address",  ui->addressEdit->text());
    query.bindValue(":age",  ui->ageSpinBox->value());
    query.bindValue(":sex",  ui->sexComboBox->currentText());
    query.bindValue(":relationship",  ui->relationshipsComboBox->currentText());

    if(query.exec())
    {
        qDebug() << "Клиент успешно добавлен.";
        clientAdded = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
        QMessageBox messageBox;
        messageBox.setText(query.lastError().text());
        messageBox.open();
    }
}

void ClientsAdd::on_cancelButton_clicked()
{
    this->close();
}
