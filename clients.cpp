#include "clients.h"
#include "ui_clients.h"

#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>
#include <QDebug>

Clients::Clients(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clients)
{
    ui->setupUi(this);

    clientsAddForm = 0;
    clientsUpdateForm = 0;

    tabelModel.setTable("clients_view");
    ui->tableView->setModel(&tabelModel);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelModel.select();

    ui->tableView->setVisible(false);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);
}

Clients::~Clients()
{
    delete ui;
}

void Clients::closeEvent(QCloseEvent* event)
{
    if(clientsAddForm != 0)
        clientsAddForm->close();

    if(clientsUpdateForm != 0)
        clientsUpdateForm->close();

    emit closed();
    QWidget::closeEvent(event);
}

void Clients::on_ClientsAddClose(bool _clientAdded)
{
    disconnect(clientsAddForm, SIGNAL(closed(bool)),
               this, SLOT(on_ClientsAddClose(bool)));

    delete clientsAddForm;
    clientsAddForm = 0;

    ui->addButton->setEnabled(true);

    if(_clientAdded)
        tabelModel.select();
}

void Clients::on_addButton_clicked()
{
    ui->addButton->setEnabled(false);

    clientsAddForm = new ClientsAdd();
    clientsAddForm->show();

    connect(clientsAddForm, SIGNAL(closed(bool)),
            this, SLOT(on_ClientsAddClose(bool)));
}

void Clients::on_deleteButton_clicked()
{
    QSqlQuery query;
    query.prepare("DELETE FROM clients WHERE id = :id");

    QVector<QString> clientsId;
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = tabelModel.index(indexes.at(i).row(), 0);
        QString id = tabelModel.data(index).toString();
        if(!clientsId.contains(id))
            clientsId += id;
    }

    for(QString &id : clientsId)
    {
        query.bindValue(":id",  id);
        if(query.exec())
        {
            qDebug() << "Клиент успешно удален. Id = " << id << ".";
        }
        else
        {
            qDebug() << query.lastError().text();
            QMessageBox messageBox;
            messageBox.setText(query.lastError().text());
            messageBox.open();
        }
    }
    tabelModel.select();
}

void Clients::on_editButton_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    if(indexes.isEmpty())
    {
        qDebug() << "Не выделено ни одной ячейки.";
        return;
    }

    ui->editButton->setEnabled(false);

    clientsUpdateForm = new ClientsUpdate();
    QModelIndex index = tabelModel.index(indexes.at(0).row(), 0);
    clientsUpdateForm->setId(tabelModel.data(index).toString());
    clientsUpdateForm->show();

    connect(clientsUpdateForm, SIGNAL(closed(bool)),
            this, SLOT(on_ClientsUpdateClose(bool)));
}

void Clients::on_ClientsUpdateClose(bool _clientUpdated)
{
    disconnect(clientsUpdateForm, SIGNAL(closed(bool)),
               this, SLOT(on_ClientsUpdateClose(bool)));

    delete clientsUpdateForm;
    clientsUpdateForm = 0;

    ui->editButton->setEnabled(true);

    if(_clientUpdated)
        tabelModel.select();
}
