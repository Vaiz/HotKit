#include "ordersadd.h"
#include "ui_ordersadd.h"

#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>
#include <QDebug>

OrdersAdd::OrdersAdd(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::OrdersAdd)
{
    ui->setupUi(this);

    selectClientForm = 0;
    selectItemForm = 0;
    clientId = "";

    QSqlQuery query;
    if(query.exec("SELECT name FROM parcell_state;"))
    {
        while (query.next())
            ui->stateComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

OrdersAdd::~OrdersAdd()
{
    if(selectClientForm != 0)
        selectClientForm->close();

    if(selectItemForm != 0)
        selectItemForm->close();

    delete ui;
}

void OrdersAdd::on_cancelButton_clicked()
{
    this->close();
}

void OrdersAdd::on_clientsAddButton_clicked()
{
    ui->clientsAddButton->setEnabled(false);

    selectClientForm = new SelectClient();
    selectClientForm->show();

    connect(selectClientForm, SIGNAL(closed(bool)),
            this, SLOT(on_SelectClientClose(bool)));
}

void OrdersAdd::on_SelectClientClose(bool _needUpdate)
{
    disconnect(selectClientForm, SIGNAL(closed(bool)),
               this, SLOT(on_SelectClientClose(bool)));

    if(_needUpdate)
    {
        clientId = selectClientForm->getClientId();

        QSqlQuery query;
        query.prepare("SELECT first_name, second_name FROM clients WHERE id = :id ;");
        query.bindValue(":id", clientId);

        if(query.exec())
        {
            if(query.next())
            {
                QString first_name = query.value("first_name").toString();
                QString second_name = query.value("second_name").toString();
                ui->clientEdit->setText(first_name + " " + second_name);
            }
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }

    delete selectClientForm;
    selectClientForm = 0;

    ui->clientsAddButton->setEnabled(true);
}

void OrdersAdd::on_addOrderButton_clicked()
{
    ui->addOrderButton->setEnabled(false);

    selectItemForm = new SelectItem();
    selectItemForm->show();

    connect(selectItemForm, SIGNAL(closed(bool)),
            this, SLOT(on_SelectItemClose(bool)));
}

void OrdersAdd::on_SelectItemClose(bool _needUpdate)
{
    disconnect(selectItemForm, SIGNAL(closed(bool)),
               this, SLOT(on_SelectItemClose(bool)));

    if(_needUpdate)
    {
        QPair<QString, QString> itemId;
        itemId.first = selectItemForm->getItemId();
        itemId.second = selectItemForm->getImageId();
        itemsId += itemId;

        QSqlQuery queryItem;
        queryItem.prepare("SELECT * FROM items_view WHERE id = :id ;");
        queryItem.bindValue(":id", itemId.first);

        QSqlQuery queryImage;
        queryImage.prepare("SELECT path FROM images WHERE id = :id ;");
        queryImage.bindValue(":id", itemId.second);

        if(queryItem.exec() && queryImage.exec())
        {
            if(queryItem.next() && queryImage.next())
            {
                QString category = queryItem.value("Категория").toString();
                QString subCategory = queryItem.value("Подкатегория").toString();
                QString technology = queryItem.value("Технология").toString();
                QString fullText = category + " / " + subCategory + " / " + technology;

                QString imagePath = queryImage.value("path").toString();

                ui->listWidget->addItem(new QListWidgetItem(QIcon(imagePath), fullText));

                int cost = queryItem.value("Цена").toInt() +
                        ui->costEdit->text().toInt();

                ui->costEdit->setText(QString::number(cost));
            }
        }
        else
        {
            qDebug() << queryItem.lastError().text();
            qDebug() << queryImage.lastError().text();
        }
    }

    delete selectItemForm;
    selectItemForm = 0;

    ui->addOrderButton->setEnabled(true);
}

void OrdersAdd::on_addButton_clicked()
{
    if(clientId == "")
    {
        qDebug() << "Выберите клиента!";
        return;
    }

    if(itemsId.isEmpty())
    {
        qDebug() << "Заказ не может быть пустым!";
        return;
    }

    QSqlQuery query;
    query.prepare("CALL add_order (:client_id , :parcell_state, @order_id);");
    query.bindValue(":client_id", clientId);
    query.bindValue(":parcell_state", ui->stateComboBox->currentText());

    if(!query.exec())
    {
        qDebug() << query.lastError().text();
        return;
    }

    if(!query.exec("SELECT @order_id;"))
    {
        qDebug() << query.lastError().text();
        return;
    }

    query.next();
    QString orderId = query.value(0).toString();

    query.clear();
    query.prepare("CALL add_orders_content (:order_id, :item_id, :image_id);");
    query.bindValue(":order_id", orderId);

    for(int i = 0; i < itemsId.count(); i++)
    {
        query.bindValue(":item_id", itemsId[i].first);
        query.bindValue(":image_id", itemsId[i].second);

        if(!query.exec())
        {
            qDebug() << query.lastError().text();

            query.clear();
            query.prepare("CALL delete_order(:order_id);");
            query.bindValue(":order_id", orderId);
            if(!query.exec())
                qDebug() << query.lastError().text();

            query.clear();
            query.prepare("CALL add_items_count(:item_id, 1);");

            for(int j = i - 1; j >= 0; j--)
            {
                query.bindValue(":item_id", itemsId[i].first);
                if(!query.exec())
                    qDebug() << query.lastError().text();
            }

            return;
        }
    }

    needUpdate = true;
    this->close();
}

void OrdersAdd::on_clearOrdersButton_clicked()
{
    ui->listWidget->clear();
    itemsId.clear();
}
