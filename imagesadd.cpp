#include "imagesadd.h"
#include "ui_imagesadd.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"
#include <QFileDialog>

ImagesAdd::ImagesAdd(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::ImagesAdd)
{
    ui->setupUi(this);

    QSqlQuery query;
    if(query.exec("SELECT name FROM hobby;"))
    {
        while (query.next())
            ui->hobbyComboBox->addItem(query.value("name").toString());

        query.prepare("SELECT name FROM sub_hobby WHERE sub_hobby.hobby_id = ( "
                      "SELECT id FROM hobby WHERE name = "
                      ":category );");
        query.bindValue(":category", ui->hobbyComboBox->currentText());

        if(query.exec())
        {
            while (query.next())
                ui->subHobbyComboBox->addItem(query.value("name").toString());
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    connect(ui->hobbyComboBox, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(on_HobbyChanged(QString)));
}

ImagesAdd::~ImagesAdd()
{
    delete ui;
}

void ImagesAdd::on_HobbyChanged(const QString &_hobby)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM sub_hobby WHERE sub_hobby.hobby_id = ( "
                  "SELECT id FROM hobby WHERE name = "
                  ":category );");
    query.bindValue(":category", _hobby);

    if(query.exec())
    {
        ui->subHobbyComboBox->clear();

        while (query.next())
            ui->subHobbyComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

void ImagesAdd::on_cancelButton_clicked()
{
    this->close();
}

void ImagesAdd::on_addImagesButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Images (*.png *.xpm *.jpg)"));
    dialog.setViewMode(QFileDialog::Detail);

    if (dialog.exec())
    {
        QStringList fileNames = dialog.selectedFiles();
        for(const QString &fileName: fileNames)
        {
            if(ui->listWidget->findItems(fileName, Qt::MatchExactly).isEmpty())
                ui->listWidget->addItem(new QListWidgetItem(QIcon(fileName), fileName));
        }
    }
}

void ImagesAdd::on_addButton_clicked()
{
    QString hobby_id("SELECT id INTO @hobby_id\n"
                     "FROM Hotkit.hobby\n"
                     "WHERE name = :hobby ;\n\n");
    QString sub_hobby_id("SELECT sub_hobby.id INTO @sub_hobby_id\n"
                         "FROM Hotkit.sub_hobby, Hotkit.hobby\n"
                         "WHERE sub_hobby.hobby_id = hobby.id\n"
                         "AND sub_hobby.name = :sub_hobby\n"
                         "AND hobby_id = @hobby_id;\n\n");
    QString insert("INSERT INTO Hotkit.images (`path`, `sub_hobby_id`) VALUES\n");

    if(ui->listWidget->count() == 0)
    {
        qDebug() << "Добавьте изображения.";
        return;
    }

    bool first = true;
    for(int i = 0; i < ui->listWidget->count(); i++)
    {
        if(!first)
            insert += ",\n";
        else
            first = false;
        QListWidgetItem *item = ui->listWidget->item(i);
        insert += "('" + item->text() + "', @sub_hobby_id)";
    }
    insert += ";";

    QSqlQuery query;
    query.prepare(hobby_id + sub_hobby_id + insert);
    query.bindValue(":hobby", ui->hobbyComboBox->currentText());
    query.bindValue(":sub_hobby", ui->subHobbyComboBox->currentText());
    if(query.exec())
    {
        qDebug() << hobby_id + sub_hobby_id + insert;
        qDebug() << "Изображения успешно добавлены.";
        needUpdate = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
        QMessageBox messageBox;
        messageBox.setText(query.lastError().text());
        messageBox.open();
    }
}

void ImagesAdd::on_clearImagesButton_clicked()
{
    ui->listWidget->clear();
}
