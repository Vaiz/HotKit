#include "subwindowstemplate.h"

SubWindowsTemplate::SubWindowsTemplate(QWidget *parent)
    : QWidget(parent)
{
    needUpdate = false;
}

void SubWindowsTemplate::closeEvent(QCloseEvent* event)
{
    emit closed(needUpdate);
    QWidget::closeEvent(event);
}
