#include "statistics.h"
#include "ui_statistics.h"

#include <QtWidgets>
#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

#include "pieview.h"

Statistics::Statistics(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::Statistics)
{
    ui->setupUi(this);

    ui->hobbyComboBox->addItem("-");
    ui->relationshipComboBox->addItem("-");
    ui->sexComboBox->addItem("-");

    QSqlQuery query;
    if(query.exec("SELECT name FROM hobby;"))
    {
        while (query.next())
            ui->hobbyComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    query.clear();
    if(query.exec("SELECT name FROM relationship;"))
    {
        while (query.next())
            ui->relationshipComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    query.clear();
    if(query.exec("SELECT name FROM sex;"))
    {
        while (query.next())
            ui->sexComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    colors += 0x0000ff;
    colors += 0x00ff00;
    colors += 0xff0000;
    colors += 0x808000;
    colors += 0x008080;
    colors += 0x800080;
    colors += 0x804040;
    colors += 0x404080;
    colors += 0x408040;
    colors += 0x8080ff;
    colors += 0xff8080;
    colors += 0x80ff80;

    splitter = 0;
    setupModel();
    setupViews();

    openView("SELECT * FROM hobby_stats_view;");
}

Statistics::~Statistics()
{
    delete ui;
}

void Statistics::resizeEvent(QResizeEvent * _event)
{
    QWidget::resizeEvent(_event);
    if(splitter != 0)
        splitter->resize(ui->widget->size());
}

void Statistics::setupModel()
{
    model = new QStandardItemModel(8, 2, this);
    model->setHeaderData(0, Qt::Horizontal, tr("Имя"));
    model->setHeaderData(1, Qt::Horizontal, tr("Количество заказов"));
}

void Statistics::setupViews()
{
    splitter = new QSplitter(ui->widget);

    QTableView *table = new QTableView;
    pieChart = new PieView;
    splitter->addWidget(table);
    splitter->addWidget(pieChart);
    splitter->setStretchFactor(0, 0);
    splitter->setStretchFactor(1, 1);

    table->setModel(model);
    pieChart->setModel(model);

    QItemSelectionModel *selectionModel = new QItemSelectionModel(model);
    table->setSelectionModel(selectionModel);
    pieChart->setSelectionModel(selectionModel);

    QHeaderView *headerView = table->horizontalHeader();
    headerView->setStretchLastSection(true);
}

void Statistics::openView(const QString &_view)
{
    model->removeRows(0, model->rowCount(QModelIndex()), QModelIndex());

    QSqlQuery query;
    if(!query.exec(_view))
    {
        qDebug() << query.lastQuery();
        qDebug() << query.lastError().text();
        return;
    }

    QVector<int>::iterator it = colors.begin();
    int row = 0;
    while(query.next())
    {
        model->insertRows(row, 1, QModelIndex());

        model->setData(model->index(row, 0, QModelIndex()),
                       query.value(0));
        model->setData(model->index(row, 1, QModelIndex()),
                       query.value(1));
        model->setData(model->index(row, 0, QModelIndex()),
                       QColor(*it++), Qt::DecorationRole);
        if(it == colors.end())
            it = colors.begin();

        row++;
    }
}


void Statistics::on_okButton_clicked()
{
    if(ui->sexComboBox->currentText() == "-" &&
            ui->relationshipComboBox->currentText() == "-" &&
            ui->hobbyComboBox->currentText() == "-")
    {
        openView("SELECT * FROM hobby_stats_view;");
        return;
    }

    if(ui->sexComboBox->currentText() == "-" &&
            ui->relationshipComboBox->currentText() == "-" &&
            ui->hobbyComboBox->currentText() != "-")
    {
        openView("SELECT `Хобби`, `Количество заказов` \n"
                 "FROM sub_hobby_stats_view, hobby \n"
                 "WHERE \n"
                 "hobby.`name` = '" + ui->hobbyComboBox->currentText() + "' \n"
                 "AND hobby.id = sub_hobby_stats_view.hobby_id;");
        return;
    }

    if(ui->sexComboBox->currentText() == "-" &&
            ui->relationshipComboBox->currentText() != "-" &&
            ui->hobbyComboBox->currentText() == "-")
    {
        openView("SELECT\n"
                 "`hobby_name`,\n"
                 "COUNT(hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 "`relationship_name` = '" + ui->relationshipComboBox->currentText() + "' \n"
                 "GROUP BY `hobby_name`;");
        return;
    }

    if(ui->sexComboBox->currentText() != "-" &&
            ui->relationshipComboBox->currentText() == "-" &&
            ui->hobbyComboBox->currentText() == "-")
    {
        openView("SELECT\n"
                 "`hobby_name`,\n"
                 "COUNT(hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 "`sex_name` = '" + ui->sexComboBox->currentText() + "' \n"
                 "GROUP BY `hobby_name`;");
        return;
    }

    if(ui->sexComboBox->currentText() != "-" &&
            ui->relationshipComboBox->currentText() != "-" &&
            ui->hobbyComboBox->currentText() == "-")
    {
        openView("SELECT\n"
                 "`hobby_name`,\n"
                 "COUNT(hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 "`sex_name` = '" + ui->sexComboBox->currentText() + "' \n"
                 "AND `relationship_name` = '" + ui->relationshipComboBox->currentText() + "' \n"
                 "GROUP BY `hobby_name`;");
        return;
    }

    if(ui->sexComboBox->currentText() != "-" &&
            ui->relationshipComboBox->currentText() != "-" &&
            ui->hobbyComboBox->currentText() != "-")
    {
        openView("SELECT\n"
                 "`sub_hobby_name`,\n"
                 "COUNT(sub_hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 "`sex_name` = '" + ui->sexComboBox->currentText() + "' \n"
                 "AND `relationship_name` = '" + ui->relationshipComboBox->currentText() + "' \n"
                 "AND `hobby_name` = '" + ui->hobbyComboBox->currentText() + "' \n"
                 "GROUP BY `sub_hobby_name`;");
        return;
    }

    if(ui->sexComboBox->currentText() != "-" &&
            ui->relationshipComboBox->currentText() == "-" &&
            ui->hobbyComboBox->currentText() != "-")
    {
        openView("SELECT\n"
                 "`sub_hobby_name`,\n"
                 "COUNT(sub_hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 "`sex_name` = '" + ui->sexComboBox->currentText() + "' \n"
                 //"AND `relationship_name` = '" + ui->relationshipComboBox->currentText() + "' \n"
                 "AND `hobby_name` = '" + ui->hobbyComboBox->currentText() + "' \n"
                 "GROUP BY `sub_hobby_name`;");
        return;
    }

    if(ui->sexComboBox->currentText() == "-" &&
            ui->relationshipComboBox->currentText() != "-" &&
            ui->hobbyComboBox->currentText() != "-")
    {
        openView("SELECT\n"
                 "`sub_hobby_name`,\n"
                 "COUNT(sub_hobby_name) AS `Количество заказов` \n"
                 "FROM id_and_names_for_stats_view\n"
                 "WHERE\n"
                 //"`sex_name` = '" + ui->sexComboBox->currentText() + "' \n"
                 "`relationship_name` = '" + ui->relationshipComboBox->currentText() + "' \n"
                 "AND `hobby_name` = '" + ui->hobbyComboBox->currentText() + "' \n"
                 "GROUP BY `sub_hobby_name`;");
        return;
    }
}
