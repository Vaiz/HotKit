-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: Hotkit
-- Source Schemata: Hotkit
-- Created: Sat May 23 15:31:33 2015
-- Workbench Version: 6.3.3
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema Hotkit
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `Hotkit` ;
CREATE SCHEMA IF NOT EXISTS `Hotkit` DEFAULT CHARACTER SET cp1251;;

-- ----------------------------------------------------------------------------
-- Table Hotkit.clients
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`clients` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(30) NOT NULL,
  `second_name` VARCHAR(30) NOT NULL,
  `zip_code` INT(11) NOT NULL,
  `sex_id` INT(11) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  `town` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `relationship_id` INT(11) NOT NULL,
  `age` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_clients_1_idx` (`sex_id` ASC),
  INDEX `fk_clients_2_idx` (`relationship_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.hobby
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`hobby` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.images
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(256) NOT NULL,
  `sub_hobby_id` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_images_1_idx` (`sub_hobby_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.item_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`item_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251
COMMENT = 'телефон, документ или вещь(зажим,кошелек,сумка)';

-- ----------------------------------------------------------------------------
-- Table Hotkit.items
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`items` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sub_item_type_id` INT(11) NOT NULL,
  `technology_id` INT(11) NOT NULL,
  `cost` INT(11) NOT NULL,
  `count` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_items_1_idx` (`technology_id` ASC),
  INDEX `fk_items_2_idx` (`sub_item_type_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.orders
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`orders` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client_id` INT(11) NOT NULL,
  `date_of_receipt` DATETIME NOT NULL,
  `parcell_state_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `orders_fk1` (`client_id` ASC),
  INDEX `fk_orders_1_idx` (`parcell_state_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.orders_content
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`orders_content` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) NOT NULL,
  `item_id` INT(11) NOT NULL,
  `image_id` INT(11) NOT NULL,
  `cost` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_content_1_idx` (`item_id` ASC),
  INDEX `orders_content_fk1` (`order_id` ASC),
  INDEX `fk_orders_content_3_idx` (`image_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.parcell_state
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`parcell_state` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.relationship
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`relationship` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.sex
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`sex` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.sub_hobby
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`sub_hobby` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `hobby_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `sub_hobby_fk1` (`hobby_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251;

-- ----------------------------------------------------------------------------
-- Table Hotkit.sub_item_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`sub_item_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `item_type_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sub_item_type_1_idx` (`item_type_id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251
COMMENT = 'марка телефона или название документа или вещь';

-- ----------------------------------------------------------------------------
-- Table Hotkit.technology
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotkit`.`technology` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = cp1251
COMMENT = '2D белый, 2D черный, 3D или принтер по коже';

-- ----------------------------------------------------------------------------
-- View Hotkit.clients_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`clients_view` AS select `Hotkit`.`clients`.`id` AS `ID`,`Hotkit`.`clients`.`first_name` AS `Фамилия`,`Hotkit`.`clients`.`second_name` AS `Имя`,`Hotkit`.`clients`.`zip_code` AS `Индекс`,`Hotkit`.`clients`.`state` AS `Область`,`Hotkit`.`clients`.`town` AS `Город`,`Hotkit`.`clients`.`address` AS `Адрес`,`Hotkit`.`clients`.`age` AS `Возраст`,`Hotkit`.`sex`.`name` AS `Пол`,`Hotkit`.`relationship`.`name` AS `Семейное положение` from ((`Hotkit`.`clients` join `Hotkit`.`sex`) join `Hotkit`.`relationship`) where ((`Hotkit`.`clients`.`sex_id` = `Hotkit`.`sex`.`id`) and (`Hotkit`.`clients`.`relationship_id` = `Hotkit`.`relationship`.`id`));

-- ----------------------------------------------------------------------------
-- View Hotkit.hobby_stats_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`hobby_stats_view` AS select `Hotkit`.`hobby`.`name` AS `Хобби`,count(`Hotkit`.`hobby`.`name`) AS `Количество заказов` from (`Hotkit`.`hobby` join `Hotkit`.`id_for_stats_view`) where (`Hotkit`.`hobby`.`id` = `id_for_stats_view`.`hobby_id`) group by `Hotkit`.`hobby`.`name`;

-- ----------------------------------------------------------------------------
-- View Hotkit.id_and_names_for_stats_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`id_and_names_for_stats_view` AS select `id_for_stats_view`.`hobby_id` AS `hobby_id`,`Hotkit`.`hobby`.`name` AS `hobby_name`,`id_for_stats_view`.`sub_hobby_id` AS `sub_hobby_id`,`Hotkit`.`sub_hobby`.`name` AS `sub_hobby_name`,`id_for_stats_view`.`sex_id` AS `sex_id`,`Hotkit`.`sex`.`name` AS `sex_name`,`id_for_stats_view`.`relationship_id` AS `relationship_id`,`Hotkit`.`relationship`.`name` AS `relationship_name` from ((((`Hotkit`.`id_for_stats_view` join `Hotkit`.`sex`) join `Hotkit`.`relationship`) join `Hotkit`.`hobby`) join `Hotkit`.`sub_hobby`) where ((`id_for_stats_view`.`hobby_id` = `Hotkit`.`hobby`.`id`) and (`id_for_stats_view`.`sub_hobby_id` = `Hotkit`.`sub_hobby`.`id`) and (`id_for_stats_view`.`sex_id` = `Hotkit`.`sex`.`id`) and (`id_for_stats_view`.`relationship_id` = `Hotkit`.`relationship`.`id`));

-- ----------------------------------------------------------------------------
-- View Hotkit.id_for_stats_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`id_for_stats_view` AS select `Hotkit`.`sub_hobby`.`hobby_id` AS `hobby_id`,`Hotkit`.`sub_hobby`.`id` AS `sub_hobby_id`,`Hotkit`.`clients`.`sex_id` AS `sex_id`,`Hotkit`.`clients`.`relationship_id` AS `relationship_id` from ((((`Hotkit`.`sub_hobby` join `Hotkit`.`images`) join `Hotkit`.`orders_content`) join `Hotkit`.`orders`) join `Hotkit`.`clients`) where ((`Hotkit`.`images`.`id` = `Hotkit`.`orders_content`.`image_id`) and (`Hotkit`.`sub_hobby`.`id` = `Hotkit`.`images`.`sub_hobby_id`) and (`Hotkit`.`orders`.`id` = `Hotkit`.`orders_content`.`order_id`) and (`Hotkit`.`clients`.`id` = `Hotkit`.`orders`.`client_id`));

-- ----------------------------------------------------------------------------
-- View Hotkit.items_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`items_view` AS select `Hotkit`.`items`.`id` AS `ID`,`Hotkit`.`item_type`.`name` AS `Категория`,`Hotkit`.`sub_item_type`.`name` AS `Подкатегория`,`Hotkit`.`technology`.`name` AS `Технология`,`Hotkit`.`items`.`count` AS `Количество`,`Hotkit`.`items`.`cost` AS `Цена` from ((`Hotkit`.`items` join (`Hotkit`.`sub_item_type` join `Hotkit`.`item_type`)) join `Hotkit`.`technology`) where ((`Hotkit`.`items`.`sub_item_type_id` = `Hotkit`.`sub_item_type`.`id`) and (`Hotkit`.`items`.`technology_id` = `Hotkit`.`technology`.`id`) and (`Hotkit`.`sub_item_type`.`item_type_id` = `Hotkit`.`item_type`.`id`));

-- ----------------------------------------------------------------------------
-- View Hotkit.orders_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`orders_view` AS select `Hotkit`.`orders`.`id` AS `ID`,concat(`Hotkit`.`clients`.`first_name`,' ',`Hotkit`.`clients`.`second_name`) AS `Заказчик`,`Hotkit`.`orders`.`date_of_receipt` AS `Дата заказа`,`Hotkit`.`parcell_state`.`name` AS `Статус`,(select sum(`Hotkit`.`orders_content`.`cost`) from `Hotkit`.`orders_content` where (`Hotkit`.`orders_content`.`order_id` = `Hotkit`.`orders`.`id`)) AS `Цена` from ((`Hotkit`.`orders` join `Hotkit`.`clients`) join `Hotkit`.`parcell_state`) where ((`Hotkit`.`orders`.`client_id` = `Hotkit`.`clients`.`id`) and (`Hotkit`.`orders`.`parcell_state_id` = `Hotkit`.`parcell_state`.`id`));

-- ----------------------------------------------------------------------------
-- View Hotkit.sub_hobby_stats_view
-- ----------------------------------------------------------------------------
USE `Hotkit`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`Tereshin`@`%` SQL SECURITY DEFINER VIEW `Hotkit`.`sub_hobby_stats_view` AS select `id_for_stats_view`.`hobby_id` AS `hobby_id`,`Hotkit`.`sub_hobby`.`name` AS `Хобби`,count(`Hotkit`.`sub_hobby`.`name`) AS `Количество заказов` from (`Hotkit`.`id_for_stats_view` join `Hotkit`.`sub_hobby`) where (`Hotkit`.`sub_hobby`.`id` = `id_for_stats_view`.`sub_hobby_id`) group by `Hotkit`.`sub_hobby`.`name`;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.add_client
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `add_client`(IN `_first_name` varchar(45),
                                                                                            IN `_second_name` varchar(45),
                                                                                            IN `_zip_code` int,
                                                                                            IN `_state` varchar(45),
                                                                                            IN `_town` varchar(45),
                                                                                            IN `_address` varchar(45),
                                                                                            IN `_age` int,
                                                                                            IN `_sex` varchar(45),
                                                                                            IN `_relationship` varchar(45))
BEGIN
	DECLARE sex_id INT;
    DECLARE relationship_id INT;
    #DECLARE id INT;
    SELECT id INTO sex_id FROM sex WHERE sex.name = _sex;
    SELECT id INTO relationship_id FROM relationship WHERE relationship.name = _relationship;
    #SET id = client.LAST_INSERT_ID() + 1;
    
    INSERT INTO `clients` (
				`first_name`,
				`second_name`,
				`zip_code`,
				`state`,
				`town`,
				`address`,
				`age`,
				`sex_id`,
				`relationship_id`)
    VALUES (_first_name,
				_second_name,
                _zip_code,
				_state,
				_town,
				_address,
				_age,
				sex_id,
				relationship_id);
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.add_items
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `add_items`(IN _sub_item_type varchar(45),
													IN _technology varchar(45),
                                                    IN _count INT,
                                                    IN _cost INT)
BEGIN

	DECLARE record_id INT;
	DECLARE _sub_item_type_id INT;
	DECLARE _technology_id INT;
    DECLARE _old_count INT;

	SET record_id = -1;

	SELECT id INTO _sub_item_type_id 
	FROM sub_item_type
	WHERE name = _sub_item_type;

	SELECT id INTO _technology_id 
	FROM technology
	WHERE name = _technology;

	SELECT id INTO record_id 
	FROM items 
	WHERE `sub_item_type_id` = _sub_item_type_id
	AND `technology_id` =  _technology_id;

	if record_id = -1 THEN
		INSERT INTO items (`sub_item_type_id`, `technology_id`, `count`, `cost`)
        VALUE (_sub_item_type_id, _technology_id, _count, _cost);
	
    ELSE
		SELECT `count` INTO _old_count
        FROM `items`
        WHERE `id` = record_id;
        
        SET _count = _old_count + _count;
        
        UPDATE `items`
        SET `count` = _count,
        cost = _cost
        WHERE `id` = record_id;
		
	END IF;

END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.add_items_count
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `add_items_count`(IN _id INT,
															IN _count INT)
BEGIN
	DECLARE _old_count INT;
    
    SELECT `count` INTO _old_count
	FROM `items`
    WHERE `id` = _id;
        
    SET _count = _old_count + _count;
        
    UPDATE `items`
    SET `count` = _count
    WHERE `id` = _id;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.add_order
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `add_order`(IN _client_id INT,
													IN _parcell_state VARCHAR(45),
                                                    OUT _order_id INT)
BEGIN
	DECLARE _parcell_state_id INT;
    
    SELECT id INTO _parcell_state_id
    FROM parcell_state
    WHERE `name` = _parcell_state;
    
    INSERT INTO orders (`client_id`, `date_of_receipt`, `parcell_state_id`)
    VALUE (_client_id, (SELECT CURRENT_TIMESTAMP), _parcell_state_id);

	SELECT LAST_INSERT_ID() INTO _order_id;

END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.add_orders_content
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `add_orders_content`(IN _order_id INT,
																IN _item_id INT,
																IN _image_id INT)
BEGIN
	DECLARE _cost INT;
    
    SELECT `cost` INTO _cost 
    FROM `items`
    WHERE `id` = _item_id;
    
    INSERT INTO orders_content (order_id, item_id, image_id, cost)
	VALUE (_order_id, _item_id, _image_id, _cost);
    
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.delete_order
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `delete_order`(IN _order_id INT)
BEGIN
	DELETE FROM `orders_content` 
    WHERE order_id = _order_id;
    
    DELETE FROM orders
    WHERE id = _order_id;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.update_client
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `update_client`(	IN `_id` INT,
															IN `_first_name` varchar(45),
															IN `_second_name` varchar(45),
															IN `_zip_code` int,
															IN `_state` varchar(45),
															IN `_town` varchar(45),
															IN `_address` varchar(45),
															IN `_age` int,
															IN `_sex` varchar(45),
															IN `_relationship` varchar(45))
BEGIN
	DECLARE sex_id INT;
    DECLARE relationship_id INT;
    
    SELECT id INTO sex_id FROM sex WHERE sex.name = _sex;
    SELECT id INTO relationship_id FROM relationship WHERE relationship.name = _relationship;
    
    UPDATE `clients`
	SET 		`first_name` = _first_name,
				`second_name` = _second_name,
				`zip_code` = _zip_code,
                `state` = _state,
                `town` = _town,
                `address` = _address,
                `age` = _age,
                `sex_id` = sex_id,
                `relationship_id` = relationship_id
	WHERE `id` = _id;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.update_items
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `update_items`(IN _id INT,
														IN _sub_item_type varchar(45),
														IN _technology varchar(45),
														IN _count INT,
                                                        IN _cost INT)
BEGIN

	DECLARE _sub_item_type_id INT;
	DECLARE _technology_id INT;

	SELECT id INTO _sub_item_type_id 
	FROM sub_item_type
	WHERE name = _sub_item_type;

	SELECT id INTO _technology_id 
	FROM technology
	WHERE name = _technology;

	UPDATE `items`
	SET 	`sub_item_type_id` = _sub_item_type_id,
			`technology_id` = _technology_id,
			`count` = _count,
            `cost` = _cost
    WHERE `id` = _id;
    
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine Hotkit.update_orders_state
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`Tereshin`@`%` PROCEDURE `update_orders_state`(IN _id INT,
																	IN _parcell_state VARCHAR(45))
BEGIN
	DECLARE _parcell_state_id INT;
    
    SELECT id INTO _parcell_state_id
    FROM parcell_state
    WHERE `name` = _parcell_state;
    
    UPDATE `orders`
    SET parcell_state_id = _parcell_state_id
    WHERE id = _id;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Trigger Hotkit.orders_content_BEFORE_INSERT
-- ----------------------------------------------------------------------------
DELIMITER $$
USE `Hotkit`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `Hotkit`.`orders_content_BEFORE_INSERT` 
BEFORE INSERT ON `orders_content` FOR EACH ROW
BEGIN
	DECLARE _count INT;
    
	SELECT `count` INTO _count 
    FROM items
    WHERE id = NEW.item_id;
    
    IF(_count = 0) THEN
		SIGNAL SQLSTATE '02000'
			SET MESSAGE_TEXT = 'Данные чехлы закончились.';
	ELSE
		UPDATE `items`
        SET `count` = (_count - 1)
        WHERE `id` = NEW.item_id;
    END IF;
END;
SET FOREIGN_KEY_CHECKS = 1;
