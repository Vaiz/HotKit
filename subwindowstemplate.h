#ifndef SUBWINDOWSTEMPLATE_H
#define SUBWINDOWSTEMPLATE_H

#include <QWidget>

class SubWindowsTemplate : public QWidget
{
    Q_OBJECT

protected:
    bool needUpdate;

public:
    explicit SubWindowsTemplate(QWidget *parent = 0);

signals:
    void closed(bool _needUpdate = false);

public slots:

protected:
    void closeEvent(QCloseEvent* event);
};

#endif // SUBWINDOWSTEMPLATE_H
