#include "selectclient.h"
#include "ui_selectclient.h"

#include <QDebug>

SelectClient::SelectClient(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::SelectClient)
{
    ui->setupUi(this);

    tabelModel.setTable("clients_view");
    ui->tableView->setModel(&tabelModel);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelModel.select();
}

SelectClient::~SelectClient()
{
    delete ui;
}

QString SelectClient::getClientId() const
{
    return clientId;
}

void SelectClient::on_cancelButton_clicked()
{
    this->close();
}

void SelectClient::on_okButton_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    if(indexes.isEmpty())
        qDebug() << "Выберите клиента!";
    else
    {
        QModelIndex index = tabelModel.index(indexes.at(0).row(), 0);
        clientId = tabelModel.data(index).toString();
        needUpdate = true;
        this->close();
    }
}
