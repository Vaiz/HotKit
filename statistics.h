#ifndef STATISTICS_H
#define STATISTICS_H

#include "subwindowstemplate.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QAbstractItemView;
class QItemSelectionModel;
class QSplitter;
QT_END_NAMESPACE

namespace Ui {
class Statistics;
}

class Statistics : public SubWindowsTemplate
{
    Q_OBJECT

public:
    explicit Statistics(QWidget *parent = 0);
    ~Statistics();

private slots:
    void on_okButton_clicked();

private:
    Ui::Statistics *ui;

private slots:
    void openView(const QString &_view = QString());

private:
    void setupModel();
    void setupViews();
    void resizeEvent(QResizeEvent * _event);

    QAbstractItemModel *model;
    QAbstractItemView *pieChart;
    QItemSelectionModel *selectionModel;
    QSplitter *splitter;
    QVector<int> colors;
};

#endif // STATISTICS_H
