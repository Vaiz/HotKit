#include "items.h"
#include "ui_items.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QDebug>

Items::Items(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Items)
{
    ui->setupUi(this);

    itemsAddForm = 0;
    itemsUpdateForm = 0;

    tabelModel.setTable("items_view");
    ui->tableView->setModel(&tabelModel);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelModel.select();

    ui->tableView->setVisible(false);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);
}

Items::~Items()
{
    delete ui;
}

void Items::closeEvent(QCloseEvent* event)
{
    if(itemsAddForm != 0)
        itemsAddForm->close();

    if(itemsUpdateForm != 0)
        itemsUpdateForm->close();

    emit closed();
    QWidget::closeEvent(event);
}

void Items::on_addButton_clicked()
{
    ui->addButton->setEnabled(false);
    itemsAddForm = new ItemsAdd();
    itemsAddForm->show();

    connect(itemsAddForm, SIGNAL(closed(bool)),
            this, SLOT(on_ItemsAddClose(bool)));
}

void Items::on_ItemsAddClose(bool _needUpdate)
{
    disconnect(itemsAddForm, SIGNAL(closed(bool)),
               this, SLOT(on_ItemsAddClose(bool)));

    delete itemsAddForm;
    itemsAddForm = 0;

    ui->addButton->setEnabled(true);

    if(_needUpdate)
        tabelModel.select();
}

void Items::on_deleteButton_clicked()
{
    QSqlQuery query;
    query.prepare("DELETE FROM items WHERE id = :id");

    QVector<QString> itemsId;
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = tabelModel.index(indexes.at(i).row(), 0);
        QString id = tabelModel.data(index).toString();
        if(!itemsId.contains(id))
            itemsId += id;
    }

    for(QString &id : itemsId)
    {
        query.bindValue(":id",  id);
        if(query.exec())
        {
            qDebug() << "Запись успешно удалена. Id = " << id << ".";
        }
        else
        {
            qDebug() << query.lastError().text();
            QMessageBox messageBox;
            messageBox.setText(query.lastError().text());
            messageBox.open();
        }
    }
    tabelModel.select();
}

void Items::on_editButton_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    if(indexes.isEmpty())
    {
        qDebug() << "Не выделено ни одной ячейки.";
        return;
    }

    ui->editButton->setEnabled(false);

    itemsUpdateForm = new ItemsUpdate();
    QModelIndex index = tabelModel.index(indexes.at(0).row(), 0);
    itemsUpdateForm->setId(tabelModel.data(index).toString());
    itemsUpdateForm->show();

    connect(itemsUpdateForm, SIGNAL(closed(bool)),
            this, SLOT(on_ItemsUpdateClose(bool)));
}

void Items::on_ItemsUpdateClose(bool _needUpdate)
{
    disconnect(itemsUpdateForm, SIGNAL(closed(bool)),
               this, SLOT(on_ItemsUpdateClose(bool)));

    delete itemsUpdateForm;
    itemsUpdateForm = 0;

    ui->editButton->setEnabled(true);

    if(_needUpdate)
        tabelModel.select();
}
