#ifndef ITEMSUPDATE_H
#define ITEMSUPDATE_H

#include <QWidget>

#include "subwindowstemplate.h"

namespace Ui {
class ItemsUpdate;
}

class ItemsUpdate :  public SubWindowsTemplate
{
    Q_OBJECT

private:
    Ui::ItemsUpdate *ui;
    QString id;

public:
    explicit ItemsUpdate(QWidget *parent = 0);
    ~ItemsUpdate();
    void setId(QString _id);

private slots:
    void on_ItemTypeChanged(const QString &_itemType);
    void on_updateButton_clicked();
    void on_cancelButton_clicked();
};

#endif // ITEMSUPDATE_H
