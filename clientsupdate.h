#ifndef CLIENTSUPDATE_H
#define CLIENTSUPDATE_H

#include <QWidget>

namespace Ui {
class ClientsUpdate;
}

class ClientsUpdate : public QWidget
{
    Q_OBJECT

public:
    explicit ClientsUpdate(QWidget *parent = 0);
    ~ClientsUpdate();
    void setId(QString _id);

private:
    Ui::ClientsUpdate *ui;
    void closeEvent(QCloseEvent* event);
    bool clientUpdated;
    QString id;

signals:
    void closed(bool _clientUpdated = false);

private slots:
    void on_cancelButton_clicked();
    void on_updateButton_clicked();
};

#endif // CLIENTSUPDATE_H
