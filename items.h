#ifndef ITEMS_H
#define ITEMS_H

#include <QWidget>
#include <QtSql/QSqlTableModel>

#include "itemsadd.h"
#include "itemsupdate.h"

namespace Ui {
class Items;
}

class Items : public QWidget
{
    Q_OBJECT

    ItemsAdd *itemsAddForm;
    ItemsUpdate *itemsUpdateForm;
    QSqlTableModel tabelModel;

public:
    explicit Items(QWidget *parent = 0);
    ~Items();

private:
    Ui::Items *ui;
    void closeEvent(QCloseEvent* event);

signals:
    void closed();

private slots:
    void on_addButton_clicked();
    void on_ItemsAddClose(bool _needUpdate);
    void on_deleteButton_clicked();
    void on_editButton_clicked();
    void on_ItemsUpdateClose(bool _needUpdate);
};

#endif // ITEMS_H
