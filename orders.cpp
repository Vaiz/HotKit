#include "orders.h"
#include "ui_orders.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QDebug>

Orders::Orders(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::Orders)
{
    ui->setupUi(this);

    ordersAddForm = 0;
    ordersUpdateForm = 0;

    tabelModel.setTable("orders_view");
    ui->tableView->setModel(&tabelModel);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelModel.select();

    ui->tableView->setVisible(false);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);
}

Orders::~Orders()
{
    if(ordersAddForm != 0)
        ordersAddForm->close();

    if(ordersUpdateForm != 0)
        ordersUpdateForm->close();

    delete ui;
}

void Orders::on_addButton_clicked()
{
    ui->addButton->setEnabled(false);

    ordersAddForm = new OrdersAdd();
    ordersAddForm->show();

    connect(ordersAddForm, SIGNAL(closed(bool)),
            this, SLOT(on_OrdersAddClose(bool)));
}

void Orders::on_OrdersAddClose(bool _needUpdate)
{
    disconnect(ordersAddForm, SIGNAL(closed(bool)),
               this, SLOT(on_OrdersAddClose(bool)));

    delete ordersAddForm;
    ordersAddForm = 0;

    ui->addButton->setEnabled(true);

    if(_needUpdate)
    {
        tabelModel.select();
    }
}

void Orders::on_deleteButton_clicked()
{
    QVector<QString> ordersId;
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = tabelModel.index(indexes.at(i).row(), 0);
        QString id = tabelModel.data(index).toString();
        if(!ordersId.contains(id))
            ordersId += id;
    }

    QSqlQuery query;
    query.prepare("CALL delete_order(:order_id);");
    for(QString &id : ordersId)
    {
        query.bindValue(":order_id",  id);
        if(query.exec())
        {
            qDebug() << "Запись успешно удалена. Id = " << id << ".";
        }
        else
        {
            qDebug() << query.lastError().text();
            QMessageBox messageBox;
            messageBox.setText(query.lastError().text());
            messageBox.open();
        }
    }
    tabelModel.select();
}

void Orders::on_editButton_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    if(indexes.isEmpty())
    {
        qDebug() << "Не выделено ни одной ячейки.";
        return;
    }

    ui->editButton->setEnabled(false);

    ordersUpdateForm = new OrdersUpdate();
    QModelIndex index = tabelModel.index(indexes.at(0).row(), 0);
    ordersUpdateForm->setId(tabelModel.data(index).toString());
    ordersUpdateForm->show();

    connect(ordersUpdateForm, SIGNAL(closed(bool)),
            this, SLOT(on_OrdersUpdateClose(bool)));
}

void Orders::on_OrdersUpdateClose(bool _needUpdate)
{
    disconnect(ordersUpdateForm, SIGNAL(closed(bool)),
               this, SLOT(on_OrdersUpdateClose(bool)));

    delete ordersUpdateForm;
    ordersUpdateForm = 0;

    ui->editButton->setEnabled(true);

    if(_needUpdate)
    {
        tabelModel.select();
    }
}
