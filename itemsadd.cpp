#include "itemsadd.h"
#include "ui_itemsadd.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"

ItemsAdd::ItemsAdd(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::ItemsAdd)
{
    ui->setupUi(this);

    QSqlQuery query;
    if(query.exec("SELECT name FROM item_type;"))
    {
        while (query.next())
            ui->itemTypeComboBox->addItem(query.value("name").toString());

        query.prepare("SELECT name FROM sub_item_type WHERE sub_item_type.item_type_id = ( "
                      "SELECT id FROM item_type WHERE name = "
                      ":category );");
        query.bindValue(":category", ui->itemTypeComboBox->currentText());

        if(query.exec())
        {
            while (query.next())
                ui->subItemTypeComboBox->addItem(query.value("name").toString());
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    if(query.exec("SELECT * FROM technology;"))
    {
        while (query.next())
            ui->technologyComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }

    connect(ui->itemTypeComboBox, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(on_ItemTypeChanged(QString)));
}

ItemsAdd::~ItemsAdd()
{
    delete ui;
}

void ItemsAdd::on_ItemTypeChanged(const QString &_itemType)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM sub_item_type WHERE sub_item_type.item_type_id = ( "
                  "SELECT id FROM item_type WHERE name = "
                  ":category );");
    query.bindValue(":category", _itemType);

    if(query.exec())
    {
        ui->subItemTypeComboBox->clear();

        while (query.next())
            ui->subItemTypeComboBox->addItem(query.value("name").toString());
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}

void ItemsAdd::on_cancelButton_clicked()
{
    this->close();
}

void ItemsAdd::on_addButton_clicked()
{
    QSqlQuery query;
    query.prepare("call Hotkit.add_items( :sub_item_type, :tecnology, :count, :cost);");
    query.bindValue(":sub_item_type", ui->subItemTypeComboBox->currentText());
    query.bindValue(":tecnology", ui->technologyComboBox->currentText());
    query.bindValue(":count", ui->countSpinBox->value());
    query.bindValue(":cost", ui->costSpinBox->value());

    if(query.exec())
    {
        needUpdate = true;
        this->close();
    }
    else
    {
        qDebug() << query.lastError().text();
    }
}
