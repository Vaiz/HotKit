#ifndef ITEMSADD_H
#define ITEMSADD_H

#include "subwindowstemplate.h"

namespace Ui {
class ItemsAdd;
}

class ItemsAdd : public SubWindowsTemplate
{
    Q_OBJECT

public:
    explicit ItemsAdd(QWidget *parent = 0);
    ~ItemsAdd();

private:
    Ui::ItemsAdd *ui;

private slots:
    void on_ItemTypeChanged(const QString &_itemType);
    void on_cancelButton_clicked();
    void on_addButton_clicked();
};

#endif // ITEMSADD_H
