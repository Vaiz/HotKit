#include "images.h"
#include "ui_images.h"

#include "QSqlQuery"
#include "QDebug"
#include "QSqlError"
#include "QMessageBox"
#include "QLabel"
#include "QIcon"
#include "QListWidget"
#include "QFileInfo"
#include "QGridLayout"

Images::Images(QWidget *parent) :
    SubWindowsTemplate(parent),
    ui(new Ui::Images)
{
    ui->setupUi(this);

    imagesAddForm = 0;

    loadImages();
}

Images::~Images()
{
    if(imagesAddForm != 0)
        imagesAddForm->close();

    delete ui;
}

void Images::on_addButton_clicked()
{
    ui->addButton->setEnabled(false);

    imagesAddForm = new ImagesAdd();
    imagesAddForm->show();

    connect(imagesAddForm, SIGNAL(closed(bool)),
            this, SLOT(on_ImagesAddClose(bool)));
}

void Images::on_ImagesAddClose(bool _needUpdate)
{
    disconnect(imagesAddForm, SIGNAL(closed(bool)),
               this, SLOT(on_ImagesAddClose(bool)));

    delete imagesAddForm;
    imagesAddForm = 0;

    ui->addButton->setEnabled(true);

    if(_needUpdate)
    {
        loadImages();
    }
}

void Images::loadImages()
{
    lastSelectedImage = 0;

    QGridLayout *gridLayout = (QGridLayout *) ui->gridLayout_3->itemAtPosition(0, 0);

    if(gridLayout != 0)
        delete gridLayout;

    gridLayout = new QGridLayout();
    ui->gridLayout_3->addLayout(gridLayout, 0, 0 );

    QSqlQuery queryHobby;

    if(queryHobby.exec("SELECT id, name FROM hobby;"))
    {
        while (queryHobby.next())
        {
            QString hobbyName = queryHobby.value("name").toString();
            QString hobbyId = queryHobby.value("id").toString();

            QSqlQuery querySubHobby;
            querySubHobby.prepare("SELECT id, name FROM sub_hobby WHERE sub_hobby.hobby_id = :hobbyId ;");
            querySubHobby.bindValue(":hobbyId", hobbyId);

            if(querySubHobby.exec())
            {
                while (querySubHobby.next())
                {
                    QLabel *label = new QLabel(hobbyName + "->" + querySubHobby.value("name").toString());
                    gridLayout->addWidget(label);

                    QString subHobbyId = querySubHobby.value("id").toString();

                    QSqlQuery queryImages;
                    queryImages.prepare("SELECT id, path FROM images WHERE sub_hobby_id = :sub_hobby_id ;");
                    queryImages.bindValue(":sub_hobby_id", subHobbyId);

                    if(queryImages.exec())
                    {
                        QListWidget * listWidget = new QListWidget();
                        listWidget->setViewMode(QListWidget::IconMode);
                        listWidget->setIconSize(QSize(200,200));
                        listWidget->setResizeMode(QListWidget::Adjust);
                        listWidget->setFixedHeight(220);

                        QObject::connect(listWidget, SIGNAL(itemClicked(QListWidgetItem*)),
                                         this, SLOT(on_imageClick(QListWidgetItem*)));

                        while (queryImages.next())
                        {
                            QString imagePath = queryImages.value("path").toString();
                            QString imageId = queryImages.value("id").toString();
                            listWidget->addItem(new QListWidgetItem(QIcon(imagePath), imageId));
                        }

                        gridLayout->addWidget(listWidget);
                    }
                    else
                    {
                        qDebug() << queryImages.lastError().text();
                    }
                }
            }
            else
            {
                qDebug() << querySubHobby.lastError().text();
            }
        }
    }
    else
    {
        qDebug() << queryHobby.lastError().text();
    }
}

void Images::on_deleteButton_clicked()
{
    if(lastSelectedImage)
    {
        QSqlQuery query;
        query.prepare("DELETE FROM images WHERE id = :id ;");
        query.bindValue(":id", lastSelectedImage->text());
        if(query.exec())
        {
            qDebug() << "Изображение удалено.";
            loadImages();
        }
        else
        {
            qDebug() << query.lastError().text();
        }
    }
}

void Images::on_imageClick(QListWidgetItem * _item)
{
    lastSelectedImage = _item;
}
