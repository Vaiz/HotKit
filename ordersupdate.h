#ifndef ORDERSUPDATE_H
#define ORDERSUPDATE_H

#include "subwindowstemplate.h"

namespace Ui {
class OrdersUpdate;
}

class OrdersUpdate : public SubWindowsTemplate
{
    Q_OBJECT

public:
    explicit OrdersUpdate(QWidget *parent = 0);
    ~OrdersUpdate();
    void setId(QString _id);

private slots:
    void on_cancelButton_clicked();

    void on_updateButton_clicked();

private:
    Ui::OrdersUpdate *ui;
    QString id;
};

#endif // ORDERSUPDATE_H
