#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QDebug"
#include "QSqlError"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    clientsForm = 0;
    itemsForm = 0;
    ordersForm = 0;
    imagesForm = 0;

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("127.0.0.1");
    db.setDatabaseName("Hotkit");
    db.setUserName("Tereshin");
    db.setPassword("1");

    if(!db.open())
    {
        qDebug() << "Не удалось подключится к бд";
        qDebug() << db.lastError().text();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_clientsButton_clicked()
{
    ui->clientsButton->setEnabled(false);
    clientsForm = new Clients();
    clientsForm->show();
    connect(clientsForm, SIGNAL(closed()),
            this, SLOT(on_ClientsClose()));
}

void MainWindow::on_ClientsClose()
{
    disconnect(clientsForm, SIGNAL(closed()),
               this, SLOT(on_ClientsClose()));
    delete clientsForm;
    clientsForm = 0;
    ui->clientsButton->setEnabled(true);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    QWidget::closeEvent(event);
    qApp->exit(0);
}

void MainWindow::on_itemsButton_clicked()
{
    ui->itemsButton->setEnabled(false);
    itemsForm = new Items();
    itemsForm->show();
    connect(itemsForm, SIGNAL(closed()),
            this, SLOT(on_ItemsClose()));
}

void MainWindow::on_ItemsClose()
{
    disconnect(itemsForm, SIGNAL(closed()),
               this, SLOT(on_ItemsClose()));
    delete itemsForm;
    itemsForm = 0;
    ui->itemsButton->setEnabled(true);
}

void MainWindow::on_ordersButton_clicked()
{
    ui->ordersButton->setEnabled(false);
    ordersForm = new Orders();
    ordersForm->show();
    connect(ordersForm, SIGNAL(closed(bool)),
            this, SLOT(on_OrdersClose()));
}

void MainWindow::on_OrdersClose()
{
    disconnect(ordersForm, SIGNAL(closed()),
               this, SLOT(on_OrdersClose()));
    delete ordersForm;
    ordersForm = 0;
    ui->ordersButton->setEnabled(true);
}

void MainWindow::on_imagesButton_clicked()
{
    ui->imagesButton->setEnabled(false);
    imagesForm = new Images();
    imagesForm->show();
    connect(imagesForm, SIGNAL(closed(bool)),
            this, SLOT(on_ImagesClose()));
}

void MainWindow::on_ImagesClose()
{
    disconnect(imagesForm, SIGNAL(closed()),
               this, SLOT(on_ImagesClose()));
    delete imagesForm;
    imagesForm = 0;
    ui->imagesButton->setEnabled(true);
}

void MainWindow::on_statisticButton_clicked()
{
    ui->statisticButton->setEnabled(false);
    statisticsForm = new Statistics();
    statisticsForm->show();
    connect(statisticsForm, SIGNAL(closed(bool)),
            this, SLOT(on_StatisticsClose()));
}

void MainWindow::on_StatisticsClose()
{
    disconnect(statisticsForm, SIGNAL(closed()),
               this, SLOT(on_StatisticsClose()));
    delete statisticsForm;
    statisticsForm = 0;
    ui->statisticButton->setEnabled(true);
}
