﻿INSERT INTO Hotkit.hobby (`name`) VALUES 
                ('Компьютерные игры'), 
                ('Футбол'), 
                ('Фильмы'),
                ('Сериалы'), 
                ('Спорт'),
                ('Хоккей'),
                ('Комиксы'),
                ('Музыка');  

SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Компьютерные игры';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'CS Go'), 
                (@hobby_id , 'Dota 2'), 
                (@hobby_id , 'World of Tanks');
        
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Футбол';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'Barselona'), 
                (@hobby_id , 'Real Madrid'), 
                (@hobby_id , 'Atletico'), 
                (@hobby_id , 'Manchester united'), 
                (@hobby_id , 'Chelsea'),
                (@hobby_id , 'Liverpool'),
                (@hobby_id , 'Arsenal'),
                (@hobby_id , 'Manchester city'),
                (@hobby_id,'Milan'),
                (@hobby_id,'Juventus'),
                (@hobby_id,'Inter'),
                (@hobby_id,'Bavaria'),
                (@hobby_id,'Borussia dortmund'), 
                (@hobby_id,'Цска'),
                (@hobby_id,'Зенит'),
                (@hobby_id,'Спартак'),
                (@hobby_id,'Динамо'),
                (@hobby_id,'@Локомотив'),
                (@hobby_id,'Рубин'),
                (@hobby_id,'Краснодар'),
                (@hobby_id,'Ростов');	
                
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Фильмы';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , '50 оттенков серого'), 
                (@hobby_id , 'Дивергент'), 
                (@hobby_id , 'Голодные игры'), 
                (@hobby_id , 'Форсаж');

SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Сериалы';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'Игра престолов'), 
                (@hobby_id , 'Ходячие мертвецы'), 
                (@hobby_id , 'Во все тяжкие'), 
                (@hobby_id , 'Теория мирового взрыва'), 
                (@hobby_id , 'Клиника'), 
                (@hobby_id , 'Американская история ужасов');	
                
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Спорт';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'Волейбол'), 
                (@hobby_id , 'Плавание'), 
                (@hobby_id , 'Бокс'), 
                (@hobby_id , 'ММА'), 
                (@hobby_id , 'Бодибилдинг');	
                
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Хоккей';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'НХЛ'), 
                (@hobby_id , 'КХЛ'), 
                (@hobby_id , 'Динамо М'), 
                (@hobby_id , 'СКА'), 
                (@hobby_id , 'ЦСКА'), 
                (@hobby_id , 'Трактор'), 
                (@hobby_id , 'Металург'), 
                (@hobby_id , 'Акбарс'), 
                (@hobby_id , 'Салават Юлаев'), 
                (@hobby_id , 'Локомотив');
                
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Комиксы';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'Железный человек'), 
                (@hobby_id , 'Халк'), 
                (@hobby_id , 'Мстители'), 
                (@hobby_id , 'Стражи галактики'), 
                (@hobby_id , 'Капитан Америка'), 
                (@hobby_id , 'Тор'), 
                (@hobby_id , 'Супермен'), 
                (@hobby_id , 'Бетмен'), 
                (@hobby_id , 'Флеш'), 
                (@hobby_id , 'Джокер'), 
                (@hobby_id , 'Люди Икс');
                
SELECT id INTO @hobby_id FROM Hotkit.hobby WHERE name = 'Музыка';
INSERT INTO Hotkit.sub_hobby (`hobby_id`,`name`) VALUES 
                (@hobby_id , 'Поп'), 
                (@hobby_id , 'К-поп'), 
                (@hobby_id , 'Rammstein'), 
                (@hobby_id , '30 seconds to mars'), 
                (@hobby_id , 'AC/DC'), 
                (@hobby_id , 'Slipknot'), 
                (@hobby_id , 'Green Day'), 
                (@hobby_id , 'Hollywood undead'), 
                (@hobby_id , 'Dead by april'), 
                (@hobby_id , 'Эминем');            